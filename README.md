### Projektbeschreibung: Nextcloud in Docker mit MariaDB

#### Was soll mit diesem Projekt erreicht werden?
Dieses Projekt zielt darauf ab, eine einfache und reproduzierbare Methode zur Einrichtung und Ausführung einer Nextcloud-Instanz mithilfe von Docker bereitzustellen. Nextcloud ist eine Open-Source-Software für das Speichern und Teilen von Dateien, die auf einem eigenen Server betrieben werden kann. Durch die Nutzung von Docker-Containern wird die Installation und Wartung vereinfacht, da alle notwendigen Komponenten isoliert und portabel sind.

#### Kann ich es benutzen? (Was sind die Voraussetzungen?)
Ja, dieses Projekt kann von jedem benutzt werden, der die folgenden Voraussetzungen erfüllt:
1. Ein System mit installiertem Docker.
2. Grundlegende Kenntnisse im Umgang mit der Kommandozeile.
3. Ausreichend Ressourcen auf dem Host-System (mindestens 2 GB RAM und 2 CPU-Kerne empfohlen).

#### Wenn ja, wie? (Wie kann ich es installieren? Was sind die Code-Snippets, die sofort funktionieren?)

##### Installation und Konfiguration

1. **Voraussetzungen sicherstellen:**
   Stellen Sie sicher, dass Docker auf Ihrem System installiert ist. Eine Installationsanleitung finden Sie [hier für Docker](https://docs.docker.com/get-docker/).

2. **Projektverzeichnis erstellen und in das Verzeichnis wechseln:**
   ```sh
   mkdir nextcloud-docker
   cd nextcloud-docker
   ```

3. **Erstellen Sie die `Dockerfile` für MariaDB (`Dockerfile_mariadb`):**
   ```dockerfile
   # Dockerfile_mariadb
   FROM mariadb:latest

   ENV MYSQL_ROOT_PASSWORD=rootpassword
   ENV MYSQL_DATABASE=nextcloud
   ENV MYSQL_USER=nextcloud
   ENV MYSQL_PASSWORD=nextcloud

   EXPOSE 3306
   ```

4. **Erstellen Sie die `Dockerfile` für Nextcloud (`Dockerfile_nextcloud`):**
   ```dockerfile
   # Dockerfile_nextcloud
   FROM nextcloud:latest

   ENV MYSQL_HOST=mariadb
   ENV MYSQL_DATABASE=nextcloud
   ENV MYSQL_USER=nextcloud
   ENV MYSQL_PASSWORD=nextcloud

   EXPOSE 80

   # Startbefehl für Nextcloud
   CMD ["apache2-foreground"]
   ```

7. **Zugriff auf Nextcloud:**
   Öffnen Sie Ihren Webbrowser und navigieren Sie zu `http://localhost:8080`. Sie sollten das Nextcloud-Setup-Interface sehen. Folgen Sie den Anweisungen auf dem Bildschirm, um die Installation abzuschließen.

##### Zusammenfassung der Schritte
1. Voraussetzungen überprüfen und sicherstellen.
2. Projektverzeichnis erstellen.
3. `Dockerfile` für MariaDB und Nextcloud erstellen.
4. Docker-Images erstellen und Container starten.
5. Nextcloud im Browser aufrufen und einrichten.

Mit diesen Anweisungen können Sie schnell und einfach eine Nextcloud-Instanz in Docker mit einer MariaDB-Datenbank einrichten und betreiben.